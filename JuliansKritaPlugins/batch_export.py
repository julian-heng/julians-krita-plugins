# SPDX-License-Identifier: GPL-3.0-only

import re

from dataclasses import dataclass
from functools import reduce
from itertools import product
from math import floor
from pathlib import Path

from PyQt5.QtCore import QSize, Qt
from PyQt5.QtWidgets import QProgressDialog
from krita import Krita, Node

from .utils.node import node_is_exportable, node_to_img, node_iter
from .utils.tool import JKPTool


class ExportRule:
    def __init__(self, id, regex, parser, default):
        self.id = id
        self.regex = re.compile(regex)
        self.parser = parser
        self.default = default

    def match(self, s):
        return self.regex.search(s)

    def parse(self, s):
        extract = self.match(s)
        return tuple(self.parser(extract[2])) if extract else self.default


@dataclass
class ExportJob:
    fname: str
    node: Node
    layer_name: str
    scaling: int
    interpolation: str
    crop: ((int, int, int, int), object)
    extension: str


class BatchExport(JKPTool):
    MENU = "Batch Export"
    INTERPOLATION_TABLE = {
        "smooth": Qt.SmoothTransformation
        , "fast": Qt.FastTransformation
    }

    def __init__(self, qwindow):
        super().__init__(qwindow)
        self.rules = {
            "s": ExportRule("s"
                , r"(s)=((?:\d+)(?:,\d+)*)"
                , lambda v: map(int, v.split(","))
                , (100,))
            , "in": ExportRule("in"
                , r"(in)=((?:smooth|fast)(?:,(?:smooth|fast))*)"
                , lambda v: v.split(",")
                , ("smooth",))
            , "c": ExportRule("c"
                , r"(c)=((?:\d+;\d+;\d+;\d+)(?:,\d+;\d+;\d+;\d+)*)"
                , lambda v: (
                    tuple(map(int, j.split(";"))) for j in v.split(",")
                )
                , ((0, 0, 100, 100),))
            , "e": ExportRule("e"
                , r"(e)=((?:png|jpg)(?:,(?:png|jpg))*)"
                , lambda v: v.split(",")
                , ("png",))
       }

    def __call__(self):
        doc = Krita.instance().activeDocument()
        if not doc:
            return

        fname = Path(doc.fileName())
        root = doc.rootNode()

        it = node_iter(root)
        it = filter(node_is_exportable, it)
        it = filter(self.node_is_marked, it)
        it = map(self.rule_process, it)
        jobs = list(ExportJob(fname, *j) for i in it for j in i)
        self.process_jobs(jobs)

    def rule_process(self, node):
        rules = self.rule_parse(node)

        scaling = rules.get("s", self.rules["s"].default)
        interpolations = rules.get("in", self.rules["in"].default)
        crops = rules.get("c", self.rules["c"].default)
        extensions = rules.get("e", self.rules["e"].default)

        cropped_imgs = {crop: node_to_img(node, crop) for crop in crops}
        cropped_imgs = tuple((k, v) for k, v in cropped_imgs.items())

        yield from product(
            (node,)
            , (self.node_get_plain_name(node),)
            , scaling
            , interpolations
            , cropped_imgs
            , extensions)

    def rule_parse(self, node):
        return {
            id: rule.parse(node.name()) for id, rule in self.rules.items()
        }

    def process_jobs(self, jobs):
        progress = QProgressDialog("Batch Exporting..."
            , "Cancel"
            , 0
            , len(jobs)
            , self.qwindow
            , Qt.WindowFlags())
        progress.setMinimumDuration(100)
        progress.setAutoReset(True)
        progress.setAutoClose(True)
        progress.setWindowModality(Qt.WindowModal)
        progress.show()
        progress.setValue(0)

        try:
            for n, job in enumerate(jobs):
                if progress.wasCanceled():
                    break
                progress.setLabelText(f"Exporting '{job.layer_name}'...")
                out_img, out_fname = self.process_job(job)
                out_img.save(str(out_fname))
                progress.setValue(n)
        finally:
            progress.setValue(len(jobs))

    def process_job(self, job):
        _, dest_img = job.crop
        if job.scaling != 100:
            sw, sh = self.calculate_scaled_size(job.scaling, dest_img.size())
            size = QSize(sw, sh)
            transform = self.INTERPOLATION_TABLE[job.interpolation]
            dest_img = dest_img.scaled(size, Qt.IgnoreAspectRatio, transform)

        dest_path = self.generate_path(job)
        return dest_img, dest_path

    def calculate_scaled_size(self, scaling, bounds):
        w = bounds.width()
        h = bounds.height()
        sw = int(floor(0.01 * scaling * w))
        sh = int(floor(0.01 * scaling * h))
        return sw, sh

    def generate_path(self, job):
        crop, _ = job.crop
        crop_name = ",".join(map(str, crop))
        fname = Path(
            f"{job.fname.stem}_{job.layer_name}"
            f"@{job.scaling}%"
            f"_c{crop_name}"
            f"_{job.interpolation}"
        ).with_suffix(f".{job.extension}")
        path = job.fname.parent.joinpath(fname)
        return path

    def node_is_marked(self, node):
        return self.rules["e"].match(node.name())

    def node_get_plain_name(self, node):
        return reduce(
            lambda s, r: r.regex.sub("", s)
            , self.rules.values()
            , node.name()
        ).strip()
