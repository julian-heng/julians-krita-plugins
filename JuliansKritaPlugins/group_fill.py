# SPDX-License-Identifier: GPL-3.0-only

from krita import Krita, Selection, InfoObject

from .utils.tool import JKPTool


class GroupFill(JKPTool):
    MENU = "Group Selection and Insert Fill Layer"

    def __call__(self):
        app = Krita.instance()
        doc = app.activeDocument()
        win = app.activeWindow()
        view = win.activeView() if win else None
        nodes = view.selectedNodes() if view else None

        if not doc or not nodes:
            return

        info = InfoObject()
        info.setProperty("color", "#ffffff")

        select = Selection()
        select.select(0, 0, doc.width(), doc.height(), 255)
        fill = doc.createFillLayer("Fill", "color", info, select)

        group = doc.createGroupLayer("Group")
        first_node = next(iter(nodes))
        parent = first_node.parentNode()
        if not parent:
            parent = doc.rootNode()

        parent.addChildNode(group, parent.childNodes()[first_node.index()])
        group.addChildNode(fill, None)
        for node in nodes[::-1]:
            group.addChildNode(node.duplicate(), None)
            node.remove()

        doc.waitForDone()
        doc.refreshProjection()
