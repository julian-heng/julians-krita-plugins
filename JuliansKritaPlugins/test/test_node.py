# SPDX-License-Identifier: GPL-3.0-only

import unittest

from unittest.mock import MagicMock

from krita import Node

from ..utils.node import node_calc_bounds_from_crop, node_is_exportable


class TestNode(unittest.TestCase):
    def test_node_is_exportable_returns(self):
        node = Node()
        node.type = MagicMock(return_value="paintlayer")
        self.assertTrue(node_is_exportable(node))

        node = Node()
        node.type = MagicMock(return_value="transparencymask")
        self.assertFalse(node_is_exportable(node))

    def test_node_calc_bounds_from_crop(self):
        self.assertEqual(
            node_calc_bounds_from_crop(
                (0, 0, 100, 100)
                , (0, 0, 100, 100)
            )
            , (0, 0, 100, 100)
        )
        self.assertEqual(
            node_calc_bounds_from_crop(
                (0, 0, 100, 100)
                , (0, 0, 50, 50)
            )
            , (0, 0, 50, 50)
        )
        self.assertEqual(
            node_calc_bounds_from_crop(
                (0, 0, 100, 100)
                , (50, 50, 100, 100)
            )
            , (50, 50, 50, 50)
        )
        self.assertEqual(
            node_calc_bounds_from_crop(
                (0, 0, 100, 100)
                , (25, 25, 75, 75)
            )
            , (25, 25, 50, 50)
        )
