# SPDX-License-Identifier: GPL-3.0-only

import unittest

from pathlib import Path
from unittest.mock import MagicMock, patch

from krita import Node
from PyQt5.QtCore import QSize

from ..batch_export import BatchExport, ExportJob, ExportRule


class TestExportRule(unittest.TestCase):
    def setUp(self):
        super(TestExportRule, self).setUp()
        self.rules = BatchExport(None).rules


class TestScaleRule(TestExportRule):
    def setUp(self):
        super(TestScaleRule, self).setUp()
        self.rule = self.rules["s"]

    def test_scale_rule_single(self):
        s = "Layer s=100"
        self.assertTrue(self.rule.match(s))
        self.assertEqual(self.rule.parse(s), (100,))

    def test_scale_rule_multiple(self):
        s = "Layer s=100,200,300"
        self.assertTrue(self.rule.match(s))
        self.assertEqual(self.rule.parse(s), (100, 200, 300))

    def test_scale_rule_default(self):
        self.assertFalse(self.rule.match(""))
        self.assertEqual(self.rule.parse(""), self.rule.default)


class TestInterpolationRule(TestExportRule):
    def setUp(self):
        super(TestInterpolationRule, self).setUp()
        self.rule = self.rules["in"]

    def test_interpolation_rule_single(self):
        s = "Layer in=smooth"
        self.assertTrue(self.rule.match(s))
        self.assertEqual(self.rule.parse(s), ("smooth",))

    def test_interpolation_rule_multiple(self):
        s = "Layer in=smooth,fast"
        self.assertTrue(self.rule.match(s))
        self.assertEqual(self.rule.parse(s), ("smooth", "fast"))

    def test_interpolation_rule_default(self):
        self.assertFalse(self.rule.match(""))
        self.assertEqual(self.rule.parse(""), self.rule.default)


class TestCropRule(TestExportRule):
    def setUp(self):
        super(TestCropRule, self).setUp()
        self.rule = self.rules["c"]

    def test_crop_rule_single(self):
        s = "Layer c=0;0;100;100"
        self.assertTrue(self.rule.match(s))
        self.assertEqual(self.rule.parse(s), ((0, 0, 100, 100),))

    def test_crop_rule_multiple(self):
        s = "Layer c=0;0;50;50,0;0;100;100"
        self.assertTrue(self.rule.match(s))
        self.assertEqual(
            self.rule.parse(s)
            , ((0, 0, 50, 50), (0, 0, 100, 100))
        )

    def test_crop_rule_default(self):
        self.assertFalse(self.rule.match(""))
        self.assertEqual(self.rule.parse(""), self.rule.default)


class TestExtensionRule(TestExportRule):
    def setUp(self):
        super(TestExtensionRule, self).setUp()
        self.rule = self.rules["e"]

    def test_extension_rule_single(self):
        s = "Layer e=png"
        self.assertTrue(self.rule.match(s))
        self.assertEqual(self.rule.parse(s), ("png",))

    def test_extension_rule_multiple(self):
        s = "Layer e=png,jpg"
        self.assertTrue(self.rule.match(s))
        self.assertEqual(self.rule.parse(s), ("png", "jpg"))

    def test_extension_rule_default(self):
        self.assertFalse(self.rule.match(""))
        self.assertEqual(self.rule.parse(""), self.rule.default)


class TestBatchExport(unittest.TestCase):
    def setUp(self):
        super(TestBatchExport, self).setUp()
        self.p_qwindow = MagicMock()
        self.p_node_to_img = patch(
            "JuliansKritaPlugins.batch_export.node_to_img"
        ).start()
        self.batch_export = BatchExport(self.p_qwindow)
        self.job_normal = ExportJob(
            Path("/path/to/file.kra")
            , None
            , "Layer"
            , 100
            , "fast"
            , ((0, 0, 100, 100), None)
            , "png"
        )
        self.job_scaled = ExportJob(
            Path("/path/to/file.kra")
            , None
            , "Layer"
            , 200
            , "fast"
            , ((0, 0, 100, 100), None)
            , "png"
        )
        self.job_cropped= ExportJob(
            Path("/path/to/file.kra")
            , None
            , "Layer"
            , 100
            , "fast"
            , ((0, 0, 50, 50), None)
            , "png"
        )
        self.job_cropped_scaled= ExportJob(
            Path("/path/to/file.kra")
            , None
            , "Layer"
            , 200
            , "fast"
            , ((0, 0, 50, 50), None)
            , "png"
        )

    def tearDown(self):
        super(TestBatchExport, self).tearDown()
        patch.stopall()

    def test_node_is_marked(self):
        node = Node()
        node.name = MagicMock(return_value="Layer e=png")
        self.assertTrue(self.batch_export.node_is_marked(node))

        node = Node()
        node.name = MagicMock(return_value="Layer s=100 e=png")
        self.assertTrue(self.batch_export.node_is_marked(node))

        node = Node()
        node.name = MagicMock(return_value="Layer s=100 e=png in=fast")
        self.assertTrue(self.batch_export.node_is_marked(node))

        node = Node()
        node.name = MagicMock(return_value="Layer")
        self.assertFalse(self.batch_export.node_is_marked(node))

        node = Node()
        node.name = MagicMock(return_value="Layer e=")
        self.assertFalse(self.batch_export.node_is_marked(node))

        node = Node()
        node.name = MagicMock(return_value="Layer e=100")
        self.assertFalse(self.batch_export.node_is_marked(node))

        node = Node()
        node.name = MagicMock(return_value="Layer e= png")
        self.assertFalse(self.batch_export.node_is_marked(node))

        node = Node()
        node.name = MagicMock(return_value="Layer s=100")
        self.assertFalse(self.batch_export.node_is_marked(node))

        node = Node()
        node.name = MagicMock(return_value="Layer s=100 in=fast")
        self.assertFalse(self.batch_export.node_is_marked(node))

    def test_node_get_plain_name(self):
        node = Node()
        node.name = MagicMock(return_value="Layer e=png")
        self.assertEqual(self.batch_export.node_get_plain_name(node), "Layer")

        node = Node()
        node.name = MagicMock(return_value="Layer e=png,jpg")
        self.assertEqual(self.batch_export.node_get_plain_name(node), "Layer")

        node = Node()
        node.name = MagicMock(return_value="Layer e=png,jpg s=100,200")
        self.assertEqual(self.batch_export.node_get_plain_name(node), "Layer")

        node = Node()
        node.name = MagicMock(return_value="Layer e=png in=fast,smooth s=500")
        self.assertEqual(self.batch_export.node_get_plain_name(node), "Layer")

        node = Node()
        node.name = MagicMock(return_value="in=fast Layer e=png c=0;0;100;100")
        self.assertEqual(self.batch_export.node_get_plain_name(node), "Layer")

        node = Node()
        node.name = MagicMock(return_value="in=fast Layer e=png 1 s=200")
        self.assertEqual(
            self.batch_export.node_get_plain_name(node)
            , "Layer  1"
        )

    def test_generate_path(self):
        result = self.batch_export.generate_path(self.job_normal)
        expected = Path("/path/to/file_Layer@100%_c0,0,100,100_fast.png")
        self.assertEqual(result, expected)

        result = self.batch_export.generate_path(self.job_scaled)
        expected = Path("/path/to/file_Layer@200%_c0,0,100,100_fast.png")
        self.assertEqual(result, expected)

        result = self.batch_export.generate_path(self.job_cropped)
        expected = Path("/path/to/file_Layer@100%_c0,0,50,50_fast.png")
        self.assertEqual(result, expected)

        result = self.batch_export.generate_path(self.job_cropped_scaled)
        expected = Path("/path/to/file_Layer@200%_c0,0,50,50_fast.png")
        self.assertEqual(result, expected)

        expected = Path("/path/to/file_Layer#")

    def test_calculate_scaled_size(self):
        bounds = QSize(100, 100)

        scaling = self.job_normal.scaling
        result = self.batch_export.calculate_scaled_size(scaling, bounds)
        expected = (100, 100)
        self.assertEqual(result, expected)

        scaling = self.job_scaled.scaling
        result = self.batch_export.calculate_scaled_size(scaling, bounds)
        expected = (200, 200)
        self.assertEqual(result, expected)

        scaling = self.job_cropped.scaling
        result = self.batch_export.calculate_scaled_size(scaling, bounds)
        expected = (100, 100)
        self.assertEqual(result, expected)

        scaling = self.job_cropped_scaled.scaling
        result = self.batch_export.calculate_scaled_size(scaling, bounds)
        expected = (200, 200)
        self.assertEqual(result, expected)

    def test_rule_process(self):
        self.p_node_to_img.return_value = None

        node = Node()
        node.name = MagicMock(return_value="Layer e=png")
        result = list(self.batch_export.rule_process(node))
        expected = 1
        self.assertEqual(len(result), expected)

        node = Node()
        node.name = MagicMock(return_value="L e=png,jpg, s=100,200,400")
        result = list(self.batch_export.rule_process(node))
        expected = 2 * 3
        self.assertEqual(len(result), expected)

        node = Node()
        node.name = MagicMock(
            return_value=(
                "L "
                "e=png,jpg "
                "s=100,200,400 "
                "c=0;0;100;100,0;0;50;50 "
                "in=fast,smooth"
            )
        )
        result = list(self.batch_export.rule_process(node))
        expected = 2 * 3 * 2 * 2
        self.assertEqual(len(result), expected)

        node = Node()
        node.name = MagicMock(
            return_value=(
                "L "
                "e=png,jpg "
                "s=100,200,400,800,1600 "
                "c=0;0;100;100,0;0;50;50,0;25;50;75 "
                "in=fast,smooth"
            )
        )
        result = list(self.batch_export.rule_process(node))
        expected = 2 * 5 * 3 * 2
        self.assertEqual(len(result), expected)
