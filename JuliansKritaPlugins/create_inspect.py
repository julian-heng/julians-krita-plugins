# SPDX-License-Identifier: GPL-3.0-only

from krita import Krita, Selection, InfoObject

from .utils.tool import JKPTool


class CreateInspect(JKPTool):
    MENU = "Create Inspect Layer"

    def __call__(self):
        app = Krita.instance()
        doc = app.activeDocument()
        if not doc:
            return

        layer = doc.activeNode()
        if not layer:
            return

        parent = layer.parentNode();
        if not parent:
            return

        select = Selection()
        select.select(0, 0, doc.width(), doc.height(), 255)

        info = InfoObject()
        info.setProperty("color", "#7f7f7f")

        # Create grayscale filter
        gray = doc.createFillLayer("Grayscale", "color", info, select)
        gray.setBlendingMode("color")

        # Create colour extract layer
        colour = doc.createFillLayer("Colours", "color", info, select)
        colour.setBlendingMode("luminize")

        # Create inspect group
        inspect_group = doc.createGroupLayer("Inspect")
        inspect_group.setPassThroughMode(True)

        inspect_group.addChildNode(colour, None)
        inspect_group.addChildNode(gray, None)

        parent.addChildNode(inspect_group, parent.childNodes()[layer.index()])
        doc.waitForDone()
        doc.refreshProjection()
