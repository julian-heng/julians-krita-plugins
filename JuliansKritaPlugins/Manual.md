# Julian's Krita Plugins

Some plugins I've written to help automate my workflow.

  - Batch export using file types, multiple sizes, scaling types and crops
  - Center current layer
  - Group and insert white background on selected layers
  - Group selected layer with the same properties, reset layer to normal


# Usage

All tools are under the 'Tools' menu.


## Batch Export

Keyboard Shortcut: <kbd>Ctrl</kbd> + <kbd>Alt</kbd> + <kbd>Shift</kbd> +
<kbd>E</kbd>

Batch export the project based on parameters on the layer. Uses the same syntax
as the Batch Exporter plugin that comes with Krita.

Options:

  * `[e=jpg,png]`\*: export format. Required for the plugin to export
  * `[s=100,200,400]`\*: scaling sizes in %
  * `[in=smooth,fast]`: image scaling type
  * `[c=0;0;100;100,0;0;50;50]`: crop dimensions in %

\* are the same syntax as the Batch Exporter plugin

### Example

Current project file is stored at `/path/to/project.kra` with a layer with the
name set as `Paint Layer e=png s=100,200`. Running the Batch Export tool will
export the layer as a .png with 1x scale and 2x scale.

This will create the following files:

  - `/path/to/project_Paint Layer@100%_c0,0,100,100_smooth.png`
  - `/path/to/project_Paint Layer@200%_c0,0,100,100_smooth.png`


## Center

Using the currently selected layer, crop to the layer, then crop to the
original image size.


## Group and Fill

Using the selected layer, group and insert a white fill layer as the background
for the group.


## Promote to Group

Using the selected layer, create a group with the same properties containing
the selected layer. Selected layer's opacity is set to opaque and blending mode
is set to normal. There should be no visual change to the image.
