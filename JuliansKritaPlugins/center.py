# SPDX-License-Identifier: GPL-3.0-only

from krita import Krita

from .utils.tool import JKPTool


class Center(JKPTool):
    MENU = "Center Layer"

    def __call__(self):
        doc = Krita.instance().activeDocument()
        if not doc:
            return

        layer = doc.activeNode()
        if not layer:
            return

        w = doc.width()
        h = doc.height()
        lb = layer.bounds()
        lx = lb.x()
        ly = lb.y()
        lw = lb.width()
        lh = lb.height()

        # TODO: find out how to use node.move while preserving history
        doc.crop(lx, ly, lw, lh)
        doc.crop(-(abs(w - lw) // 2), -(abs(h - lh) // 2), w, h)
