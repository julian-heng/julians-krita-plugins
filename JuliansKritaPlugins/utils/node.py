# SPDX-License-Identifier: GPL-3.0-only

from math import floor

from PyQt5.QtGui import QImage


def node_iter(node):
    for child in node.childNodes():
        yield child
        yield from node_iter(child)

def node_to_img(node, crop=(0, 0, 100, 100)):
    nb = node.bounds()
    bounds = (nb.x(), nb.y(), nb.width(), nb.height())
    x, y, w, h = node_calc_bounds_from_crop(bounds, crop)
    pixel_data = node.projectionPixelData(x, y, w, h).data()
    return QImage(pixel_data, w, h, QImage.Format_ARGB32)

def node_calc_bounds_from_crop(bounds, crop):
    x, y, w, h = bounds
    cx, cy, cx2, cy2 = crop
    cw, ch = cx2 - cx, cy2 - cy
    x = max(x, int(floor(0.01 * cx * w)))
    y = max(y, int(floor(0.01 * cy * h)))
    w = min(w, int(floor(0.01 * cw * w)))
    h = min(h, int(floor(0.01 * ch * h)))
    return x, y, w, h

def node_is_exportable(node):
    exportable = {
        "paintlayer"
        , "grouplayer"
        , "filelayer"
        , "vectorlayer"
        , "clonelayer"
    }
    return node.type() in exportable
