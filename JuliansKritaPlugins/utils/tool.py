# SPDX-License-Identifier: GPL-3.0-only

from abc import ABC, abstractmethod


class JKPTool(ABC):

    def __init__(self, qwindow):
        super().__init__()
        self.qwindow = qwindow

    @property
    @abstractmethod
    def MENU(self):
        pass

    def create_action(self, window, plugin_name):
        action = window.createAction(
            f"{plugin_name}_{self.__class__.__name__}"
            , self.MENU
            , f"tools/{plugin_name}"
        )
        action.triggered.connect(self)

    @abstractmethod
    def __call__(self):
        pass
