# SPDX-License-Identifier: GPL-3.0-only

from krita import Extension, Krita
from PyQt5.QtWidgets import QMenu

from .batch_export import BatchExport
from .center import Center
from .group_fill import GroupFill
from .group_promote import GroupPromote
from .create_inspect import CreateInspect


class JKP(Extension):
    NAME = "Julian's Krita Plugins"
    NAME_SHORT = "JKP"

    def __init__(self, parent):
        super().__init__(parent)

    def setup(self):
        pass

    def createActions(self, window):
        qwin = window.qwindow()

        self.menu = QMenu(JKP.NAME_SHORT, qwin)
        self.action = window.createAction(JKP.NAME_SHORT, JKP.NAME, "tools")
        self.action.setMenu(self.menu)

        self.batch_export = BatchExport(qwin)
        self.center = Center(qwin)
        self.group_fill = GroupFill(qwin)
        self.group_promote = GroupPromote(qwin)
        self.create_inspect = CreateInspect(qwin)

        self.batch_export.create_action(window, JKP.NAME_SHORT)
        self.center.create_action(window, JKP.NAME_SHORT)
        self.group_fill.create_action(window, JKP.NAME_SHORT)
        self.group_promote.create_action(window, JKP.NAME_SHORT)
        self.create_inspect.create_action(window, JKP.NAME_SHORT)


Krita.instance().addExtension(JKP(Krita.instance()))
