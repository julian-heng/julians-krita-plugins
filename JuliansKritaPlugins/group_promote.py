# SPDX-License-Identifier: GPL-3.0-only

from krita import Krita

from .utils.tool import JKPTool


class GroupPromote(JKPTool):
    MENU = "Promote Selected Layer to Group"

    def __call__(self):
        doc = Krita.instance().activeDocument()
        if not doc:
            return

        layer = doc.activeNode()
        if not layer:
            return

        group = doc.createGroupLayer("")

        no_layer_style = group.layerStyleToAsl()

        group.setAlphaLocked(layer.alphaLocked())
        group.setBlendingMode(layer.blendingMode())
        group.setColorLabel(layer.colorLabel())
        group.setInheritAlpha(layer.inheritAlpha())
        group.setLayerStyleFromAsl(layer.layerStyleToAsl())
        group.setName(f"{layer.name()} Promoted")
        group.setOpacity(layer.opacity())

        layer_dupe = layer.duplicate()
        layer_dupe.setAlphaLocked(False)
        layer_dupe.setBlendingMode("Normal")
        layer_dupe.setInheritAlpha(False)
        layer_dupe.setLayerStyleFromAsl(no_layer_style)
        layer_dupe.setOpacity(255)

        parent = layer.parentNode()
        if not parent:
            parent = doc.rootNode()

        group.addChildNode(layer_dupe, None)
        parent.addChildNode(group, parent.childNodes()[layer.index()])
        layer.remove()
        doc.waitForDone()
        doc.refreshProjection()
