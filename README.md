# Julian's Krita Plugins

Some plugins I've written to help automate my workflow.

# Installation

## Using the Python Plugin Importer [Preferred]

1. On GitLab, click on 'Download source code' and copy the zip download link
2. On Krita, navigate to Tools > Scripts > Import Python Plugin from Web...


## Manual Installation

1. On GitLab, click on 'Download source code' and download the zip
2. Extract the zip file
3. On Krita, navigate to Settings > Manage Resources
4. Click on 'Open Resource Folder'
5. Copy `JuliansKritaPlugins` and `JuliansKritaPlugins.desktop` to `pykrita`
6. Copy `JuliansKritaPlugins.actions` to `actions`


# Development

Ideally, I normally prototype anything using the built in Scripter before
implementing it in the plugin. This saves time in not needed to reload Krita
every time a file gets modified. To ease development, I symlink the files from
the git repo to the resource folders.


## Development installation

Linux/Mac instructions only, sorry Windows users.

1. Clone the repository
2. Symlink `JuliansKritaPlugins` and `JuliansKritaPlugins.desktop` to `pykrita`
3. Symlink `JuliansKritaPlugins.action` to `actions`


## Unit tests

This project contains unit tests. PyQt5 needs to be installed on your system as
well as all the test dependencies in the `test-requirements.txt` file.

1. Install PyQt5 using your package manager.
2. Setup the virtual environment. `python3 -m venv .venv`
3. Activate the environment. `source .venv/bin/activate`
4. Install test dependencies. `pip3 -r test-requirements.txt`
5. Run unit tests. `python3 -m unittest -v`


## CICD

This project contains CICD, which only runs the unit test suite.


# Usage

All tools are under the 'Tools' menu.


## Batch Export

Keyboard Shortcut: <kbd>Ctrl</kbd> + <kbd>Alt</kbd> + <kbd>Shift</kbd> +
<kbd>E</kbd>

Batch export the project based on parameters on the layer. Uses the same syntax
as the Batch Exporter plugin that comes with Krita.

This is a watered down version of the Batch Export plugin that comes with
Krita. I written my own implementation because there was no option to export
using nearest neighbour scaling or cropping.

Options:

  * `[e=jpg,png]`\*: export format. Required for the plugin to export
  * `[s=100,200,400]`\*: scaling sizes in %
  * `[in=smooth,fast]`: image scaling type
  * `[c=0;0;100;100,0;0;50;50]`: crop dimensions in %

\* are the same syntax as the Batch Exporter plugin


### Example

Current project file is stored at `/path/to/project.kra` with a layer with the
name set as `Paint Layer e=png s=100,200`. Running the Batch Export tool will
export the layer as a .png with 1x scale and 2x scale.

This will create the following files:

  - `/path/to/project_Paint Layer@100%_c0,0,100,100_smooth.png`
  - `/path/to/project_Paint Layer@200%_c0,0,100,100_smooth.png`


### TODO

  - Optimise by caching the scaled image rather than rescaling each time
  - Cleanup file names as they include the default parameters


## Center

Using the currently selected layer, crop to the layer, then crop to the
original image size.


### TODO

  - Take into account of multiple selected layers instead of the top most
    selected one
  - Use Node.move instead of cropping image eventually


## Group and Fill

Using the selected layer, group and insert a white fill layer as the background
for the group.


## Promote to Group

Using the selected layer, create a group with the same properties containing
the selected layer. Selected layer's opacity is set to opaque and blending mode
is set to normal. There should be no visual change to the image.
